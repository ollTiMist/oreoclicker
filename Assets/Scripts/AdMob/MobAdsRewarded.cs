using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class MobAdsRewarded : MonoBehaviour
{
    private RewardedAd rewardedAd;

    [SerializeField] GameController gameController;

#if UNITY_ANDROID
    private const string rewardedUnitId = "	ca-app-pub-3940256099942544/5224354917"; //test id
#elif UNITY_IPHONE
    private const string rewardedUnitId = "";
#else
    private const string rewardedUnitId = "unexpected_platform";
#endif
    void OnEnable()
    {
        rewardedAd = new RewardedAd(rewardedUnitId);
        AdRequest adRequest = new AdRequest.Builder().Build();
        rewardedAd.LoadAd(adRequest);

        rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
    }

    public void ShowAd()
    {
        if (rewardedAd.IsLoaded())
        {
            rewardedAd.Show();
        }
    }

    private void HandleUserEarnedReward(object sender, Reward e)
    {
        gameController.score += 10;
    }
}
