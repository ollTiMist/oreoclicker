using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OnDestroy();
    }

    //Destroy this GameObject
    public void OnDestroy()
    {
        Destroy(this.gameObject, 2f);
    }
}
