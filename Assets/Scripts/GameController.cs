using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameController : MonoBehaviour
{
    public int score;
    public int scoreIncrease = 1;

    public Text scoreText;

    //Scripts
    [SerializeField]
    private MobAdsSimple mobAd;
    [SerializeField]
    private Item item;
 
    //GameObjects
    [SerializeField] 
    private GameObject ShopPanel;
    [SerializeField]
    private Slider[] slider;

    // Start is called before the first frame update
    void Start()
    {
        score = PlayerPrefs.GetInt("score", score);
        scoreIncrease = PlayerPrefs.GetInt("score_click", scoreIncrease);

        scoreText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        PlayerPrefs.SetInt("score_click", scoreIncrease);

        maxUpgradeLvl();
    }

    void FixedUpdate()
    {
        Camera.main.backgroundColor = Color.Lerp(Color.red, Color.yellow, Mathf.PingPong(Time.time, 1));
    }

    private void maxUpgradeLvl()
    {
        
        /*for (int i = 0; i < slider.Length; i++)
        {
            slider[i].value = item.levelOfItem;
            if (slider[i].value == slider[i].maxValue)
            {
                shopController.shopBttns[i].interactable = false;
            }
        }
        //Debug.Log(slider.Length);*/
    }

    public void shopPan_ShowAndHide()
    {
        ShopPanel.SetActive(!ShopPanel.activeSelf);
    }


}
