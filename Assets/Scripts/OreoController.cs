using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreoController : MonoBehaviour
{
    public GameController gameController;
    public RandomSpawnController randomSpawnController;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        gameController.score += gameController.scoreIncrease;
        animator.SetTrigger("Click");
        randomSpawnController.instantiateObject();
        PlayerPrefs.SetInt("score", gameController.score);
    }
}
