using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject OreoObject;
    float RandX;
    Vector2 whereToSpawn;

    [SerializeField]
    private GameObject spawn;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void instantiateObject()
    {
        RandX = Random.Range(-2.565f, 2.565f);
        whereToSpawn = new Vector2(RandX, transform.position.y);
        Instantiate(OreoObject, whereToSpawn, Quaternion.identity);
    }
}
