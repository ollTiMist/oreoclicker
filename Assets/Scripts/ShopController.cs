using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShopController : MonoBehaviour
{
    [Header("Shop")]
    public List<Item> shopItems = new List<Item>();
    [Header("Button text")]
    public Text[] shopItemsText;
    [Header("Shop Buttons")]
    public Button[] shopBttns;

    //Scripts
    [SerializeField]
    private GameController gameController;

    private void Start()
    {
        updateCosts(); //�������� ����� � ������
        StartCoroutine(BonusPerSec()); //��������� ������� ������ � �������
    }

    public void BuyBttn(int index) //����� ��� ������� �� ������ ������� ������ (������ ������)
    {
        int cost = shopItems[index].cost * shopItems[shopItems[index].itemIndex].bonusCounter; //������������ ���� � ����������� �� ���-�� ������� (� �������)
        if (shopItems[index].itsBonus && gameController.score >= cost) //���� ����� ������� ������ - ��� �����, � ����� >= ����(�)
        {
            if (cost > 0) // ���� ���� ������ ��� 0, ��:
            {
                gameController.score -= cost; // �������� ���� �� �����
                StartCoroutine(BonusTimer(shopItems[index].timeOfBonus, index)); //��������� �������� ������
            }
            else print("������ �������� ��!"); // ����� ������� � ������� �����.
        }
        else if (gameController.score >= shopItems[index].cost) // �����, ���� ����� ������� ������ - ��� �� �����, � ����� >= ����
        {
            if (shopItems[index].itsItemPerSec) shopItems[index].bonusCounter++; // ���� �������� �������� (� �������), �� ���������� ���-�� �������
            else gameController.scoreIncrease += shopItems[index].bonusIncrease; // ����� ������ ��� ����� ��������� ����� ������
            gameController.score -= shopItems[index].cost; // �������� ���� �� �����
            if (shopItems[index].needCostMultiplier) shopItems[index].cost *= shopItems[index].costMultiplier; // ���� ������ ����� �������� ����, �� �������� �� ���������
            shopItems[index].levelOfItem.value++; // ��������� ������� �������� �� 1
            shopItems[index].levelOfItemText.text = shopItems[index].levelOfItem.value.ToString();

            //Debug.Log(shopItems[index].levelOfItem);
        }
        else print("�� ������� �����!"); // ����� ���� 2 �������� ����� false, �� ������� � ������� �����.
        updateCosts(); //�������� ����� � ������
    }

    private void updateCosts() // ����� ��� ���������� ������ � ������
    {
        for (int i = 0; i < shopItems.Count; i++) // ���� �����������, ���� ���������� i < ���-�� �������
        {
            if (shopItems[i].itsBonus) // ���� ����� �������� �������, ��:
            {
                int cost = shopItems[i].cost * shopItems[shopItems[i].itemIndex].bonusCounter; // ������������ ���� � ����������� �� ���-�� ������� (� �������)
                shopItemsText[i].text = shopItems[i].name + cost; // ��������� ����� ������ � ������������ �����
            }
            else shopItemsText[i].text = shopItems[i].name + shopItems[i].cost; // ����� ���� ����� �� �������� �������, �� ��������� ����� ������ � ������� �����
        }
    }

    IEnumerator BonusPerSec() // ����� � �������
    {
        while (true) // ����������� ����
        {
            for (int i = 0; i < shopItems.Count; i++) gameController.score += (shopItems[i].bonusCounter * shopItems[i].bonusPerSec); // ��������� � ������� ������ ����� ������� (� �������)
            yield return new WaitForSeconds(1); // ������ �������� � 1 �������
        }
    }

    IEnumerator BonusTimer(float time, int index) // �������� ������ (������������ ������ (� ���.),������ ������)
    {
        shopBttns[index].interactable = false; // ��������� �������������� ������ ������
        shopItems[shopItems[index].itemIndex].bonusPerSec *= 2; // ��������� ����� ������� � ������� (� �������)
        yield return new WaitForSeconds(time); // ������ �������� �� ������� ������, ������� ������� � ���������
        shopItems[shopItems[index].itemIndex].bonusPerSec /= 2; // ���������� ����� � ���������� ���������
        shopBttns[index].interactable = true; // �������� �������������� ������ ������
    }
}

[Serializable]
public class Item // ����� ������
{
    [Tooltip("�������� ������������ �� �������")]
    public string name;
    [Tooltip("���� ������")]
    public int cost;
    [Tooltip("�����, ������� ����������� � ������ ��� �����")]
    public int bonusIncrease;
    public Slider levelOfItem; // ������� ������
    public Text levelOfItemText; 
    [Space]
    [Tooltip("����� �� ��������� ��� ����?")]
    public bool needCostMultiplier;
    [Tooltip("��������� ��� ����")]
    public int costMultiplier;
    [Space]
    [Tooltip("���� ����� ��� ����� � �������?")]
    public bool itsItemPerSec;
    [Tooltip("�����, ������� ����� � �������")]
    public int bonusPerSec;
    [HideInInspector]
    public int bonusCounter;
    [Space]
    [Tooltip("��� ��������� �����?")]
    public bool itsBonus;
    [Tooltip("��������� ������, ������� ����������� ������� (���������� ���������� bonusPerSec)")]
    public int itemMultiplier;
    [Tooltip("������ ������, ������� ����� ����������� ������� (���������� ���������� bonusPerSec ����� ������)")]
    public int itemIndex;
    [Tooltip("������������ ������")]
    public float timeOfBonus;
}
