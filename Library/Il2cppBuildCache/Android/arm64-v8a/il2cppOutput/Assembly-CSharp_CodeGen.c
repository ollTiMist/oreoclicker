﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MobAdInitialize::Awake()
extern void MobAdInitialize_Awake_mC56F8A5E6E461C1BF93D7B0438619492EE0E0B00 (void);
// 0x00000002 System.Void MobAdInitialize::.ctor()
extern void MobAdInitialize__ctor_mD2158A389BA8D06334EC7D179C26E33778D48D86 (void);
// 0x00000003 System.Void MobAdInitialize/<>c::.cctor()
extern void U3CU3Ec__cctor_m1F000AD1C8D8A3718CF8F1EA584673015A6EFF8C (void);
// 0x00000004 System.Void MobAdInitialize/<>c::.ctor()
extern void U3CU3Ec__ctor_mE5D71621E91C7A72E8759B96E6744D3E90A1C4AB (void);
// 0x00000005 System.Void MobAdInitialize/<>c::<Awake>b__0_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CAwakeU3Eb__0_0_m53AD37D1D7D5B81B1E5B294DA177C0769851B9E4 (void);
// 0x00000006 System.Void MobAdsRewarded::OnEnable()
extern void MobAdsRewarded_OnEnable_mBB1A66DF66D392ED3CCF61C494A51E51AFDBA9C7 (void);
// 0x00000007 System.Void MobAdsRewarded::ShowAd()
extern void MobAdsRewarded_ShowAd_mAC848AD58743EB27F394D09675DF7C9B24FD4481 (void);
// 0x00000008 System.Void MobAdsRewarded::HandleUserEarnedReward(System.Object,GoogleMobileAds.Api.Reward)
extern void MobAdsRewarded_HandleUserEarnedReward_mEA2BD5298218240827D9B157BDC5456CC82FC8ED (void);
// 0x00000009 System.Void MobAdsRewarded::.ctor()
extern void MobAdsRewarded__ctor_m62D450D547C2A374C1558E124DABE43D9448D74C (void);
// 0x0000000A System.Void MobAdsSimple::OnEnable()
extern void MobAdsSimple_OnEnable_m0299F238E3492DA27C43539ED6532997EDF6EFF8 (void);
// 0x0000000B System.Void MobAdsSimple::ShowAd()
extern void MobAdsSimple_ShowAd_m3162A00F38FB6242CB9B65FA781D0DD00F90D5B6 (void);
// 0x0000000C System.Void MobAdsSimple::.ctor()
extern void MobAdsSimple__ctor_m07A1D3386F6195832692147233A224D28F437EB3 (void);
// 0x0000000D System.Void Destroy::Start()
extern void Destroy_Start_mD6030ECFA50EF5D274A3EE80E06A1FFF224DBA0F (void);
// 0x0000000E System.Void Destroy::Update()
extern void Destroy_Update_mE27F3C49527B2120646F86A1B8E4F4BB931F5C5F (void);
// 0x0000000F System.Void Destroy::OnDestroy()
extern void Destroy_OnDestroy_m38BDCA19DE6FFF481E5C039187083E04B94CA79A (void);
// 0x00000010 System.Void Destroy::.ctor()
extern void Destroy__ctor_m487225D87A53D11346B6FD7BFAC8B4B3C33BC02E (void);
// 0x00000011 System.Void GameController::Start()
extern void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (void);
// 0x00000012 System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x00000013 System.Void GameController::FixedUpdate()
extern void GameController_FixedUpdate_mDFDE23ACB24D8EF9D60C2E2838A4C5E17C178894 (void);
// 0x00000014 System.Void GameController::maxUpgradeLvl()
extern void GameController_maxUpgradeLvl_mB2D843229BE2B4933CDBC7978AEC1C0DECE74119 (void);
// 0x00000015 System.Void GameController::shopPan_ShowAndHide()
extern void GameController_shopPan_ShowAndHide_m635C0D3E83E6CA568E9F6CC8E36E61559A678B0D (void);
// 0x00000016 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
// 0x00000017 System.Void OreoController::Start()
extern void OreoController_Start_mCDC0550B1C7DFAFC6481E9E354B948762C51B2A4 (void);
// 0x00000018 System.Void OreoController::Update()
extern void OreoController_Update_mC6A518C8F850A119013B27F0815F1F7EC0CB64CF (void);
// 0x00000019 System.Void OreoController::OnClick()
extern void OreoController_OnClick_m654CD1CEF517E484163166CECE25228569C2B1D2 (void);
// 0x0000001A System.Void OreoController::.ctor()
extern void OreoController__ctor_m95EDA3B5097F1D2C755A335F2F61A2B012FAC5A0 (void);
// 0x0000001B System.Void RandomSpawnController::Start()
extern void RandomSpawnController_Start_m6A30786538EE2CE01AF188B27780113E2BB20BAF (void);
// 0x0000001C System.Void RandomSpawnController::Update()
extern void RandomSpawnController_Update_mED3A183D861F93E7D015C1CF0C047E58EED6F4F1 (void);
// 0x0000001D System.Void RandomSpawnController::instantiateObject()
extern void RandomSpawnController_instantiateObject_m0B5BFA3190CF8EB9C939409B2DF873F2E0E99EDE (void);
// 0x0000001E System.Void RandomSpawnController::.ctor()
extern void RandomSpawnController__ctor_mD01F2F15948E46CC99EE122596FDB5ED8C0205FB (void);
// 0x0000001F System.Void ShopController::Start()
extern void ShopController_Start_m55C7E8A993F5C4F493FC48EE067711BFC57246E9 (void);
// 0x00000020 System.Void ShopController::BuyBttn(System.Int32)
extern void ShopController_BuyBttn_mA1236073BDB22FA005B184480B4C5A62F1DA054A (void);
// 0x00000021 System.Void ShopController::updateCosts()
extern void ShopController_updateCosts_m1F4C3D4FDA99242EC8D721EFA423184788F75995 (void);
// 0x00000022 System.Collections.IEnumerator ShopController::BonusPerSec()
extern void ShopController_BonusPerSec_m0DF804B193DCC1BA89DF882D1FA96FAFD2FDD973 (void);
// 0x00000023 System.Collections.IEnumerator ShopController::BonusTimer(System.Single,System.Int32)
extern void ShopController_BonusTimer_m588628945F6E4C46225F5AF23D386D6AB50F814D (void);
// 0x00000024 System.Void ShopController::.ctor()
extern void ShopController__ctor_m8F7837C4274D81D87B0F80181A8899F6B2F1D89C (void);
// 0x00000025 System.Void ShopController/<BonusPerSec>d__7::.ctor(System.Int32)
extern void U3CBonusPerSecU3Ed__7__ctor_mB51EC4AACACE6399AA67627C151CE567500964CA (void);
// 0x00000026 System.Void ShopController/<BonusPerSec>d__7::System.IDisposable.Dispose()
extern void U3CBonusPerSecU3Ed__7_System_IDisposable_Dispose_m100D6EFDE3BB4C7041A520BB706BDF8DCBD043F6 (void);
// 0x00000027 System.Boolean ShopController/<BonusPerSec>d__7::MoveNext()
extern void U3CBonusPerSecU3Ed__7_MoveNext_m44FABE968824F26D7C31C2318F91FC2AFC98D71C (void);
// 0x00000028 System.Object ShopController/<BonusPerSec>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBonusPerSecU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32F8CE94DADBD0B73237BDBB88E6C8DBAD092BF0 (void);
// 0x00000029 System.Void ShopController/<BonusPerSec>d__7::System.Collections.IEnumerator.Reset()
extern void U3CBonusPerSecU3Ed__7_System_Collections_IEnumerator_Reset_m22705D7061BB941315D77A0BD11EB0F7067C4991 (void);
// 0x0000002A System.Object ShopController/<BonusPerSec>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CBonusPerSecU3Ed__7_System_Collections_IEnumerator_get_Current_m63707780812E44139CB2E43B9EE52502FF906B74 (void);
// 0x0000002B System.Void ShopController/<BonusTimer>d__8::.ctor(System.Int32)
extern void U3CBonusTimerU3Ed__8__ctor_mAB9CE4CA8BEC4CEF3F4C81486EBB28764BB6C1FA (void);
// 0x0000002C System.Void ShopController/<BonusTimer>d__8::System.IDisposable.Dispose()
extern void U3CBonusTimerU3Ed__8_System_IDisposable_Dispose_mAB814BA2B5EA4797756E42A154A0939FD72DE392 (void);
// 0x0000002D System.Boolean ShopController/<BonusTimer>d__8::MoveNext()
extern void U3CBonusTimerU3Ed__8_MoveNext_m9B883AE257FBAA7536DCDEF158DC864B5D142F9C (void);
// 0x0000002E System.Object ShopController/<BonusTimer>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBonusTimerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CF0C7A706512AFDEB1B772A2DB5C981607330B1 (void);
// 0x0000002F System.Void ShopController/<BonusTimer>d__8::System.Collections.IEnumerator.Reset()
extern void U3CBonusTimerU3Ed__8_System_Collections_IEnumerator_Reset_m018D57EA229817C0FC5BDF4CC893C2FC828C16F3 (void);
// 0x00000030 System.Object ShopController/<BonusTimer>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CBonusTimerU3Ed__8_System_Collections_IEnumerator_get_Current_mBBD460F888EAF59B85FFD94F2A3342BBFD64807B (void);
// 0x00000031 System.Void Item::.ctor()
extern void Item__ctor_m51C5ECE57526347477E1E2A4CF519CF555BC8147 (void);
static Il2CppMethodPointer s_methodPointers[49] = 
{
	MobAdInitialize_Awake_mC56F8A5E6E461C1BF93D7B0438619492EE0E0B00,
	MobAdInitialize__ctor_mD2158A389BA8D06334EC7D179C26E33778D48D86,
	U3CU3Ec__cctor_m1F000AD1C8D8A3718CF8F1EA584673015A6EFF8C,
	U3CU3Ec__ctor_mE5D71621E91C7A72E8759B96E6744D3E90A1C4AB,
	U3CU3Ec_U3CAwakeU3Eb__0_0_m53AD37D1D7D5B81B1E5B294DA177C0769851B9E4,
	MobAdsRewarded_OnEnable_mBB1A66DF66D392ED3CCF61C494A51E51AFDBA9C7,
	MobAdsRewarded_ShowAd_mAC848AD58743EB27F394D09675DF7C9B24FD4481,
	MobAdsRewarded_HandleUserEarnedReward_mEA2BD5298218240827D9B157BDC5456CC82FC8ED,
	MobAdsRewarded__ctor_m62D450D547C2A374C1558E124DABE43D9448D74C,
	MobAdsSimple_OnEnable_m0299F238E3492DA27C43539ED6532997EDF6EFF8,
	MobAdsSimple_ShowAd_m3162A00F38FB6242CB9B65FA781D0DD00F90D5B6,
	MobAdsSimple__ctor_m07A1D3386F6195832692147233A224D28F437EB3,
	Destroy_Start_mD6030ECFA50EF5D274A3EE80E06A1FFF224DBA0F,
	Destroy_Update_mE27F3C49527B2120646F86A1B8E4F4BB931F5C5F,
	Destroy_OnDestroy_m38BDCA19DE6FFF481E5C039187083E04B94CA79A,
	Destroy__ctor_m487225D87A53D11346B6FD7BFAC8B4B3C33BC02E,
	GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController_FixedUpdate_mDFDE23ACB24D8EF9D60C2E2838A4C5E17C178894,
	GameController_maxUpgradeLvl_mB2D843229BE2B4933CDBC7978AEC1C0DECE74119,
	GameController_shopPan_ShowAndHide_m635C0D3E83E6CA568E9F6CC8E36E61559A678B0D,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
	OreoController_Start_mCDC0550B1C7DFAFC6481E9E354B948762C51B2A4,
	OreoController_Update_mC6A518C8F850A119013B27F0815F1F7EC0CB64CF,
	OreoController_OnClick_m654CD1CEF517E484163166CECE25228569C2B1D2,
	OreoController__ctor_m95EDA3B5097F1D2C755A335F2F61A2B012FAC5A0,
	RandomSpawnController_Start_m6A30786538EE2CE01AF188B27780113E2BB20BAF,
	RandomSpawnController_Update_mED3A183D861F93E7D015C1CF0C047E58EED6F4F1,
	RandomSpawnController_instantiateObject_m0B5BFA3190CF8EB9C939409B2DF873F2E0E99EDE,
	RandomSpawnController__ctor_mD01F2F15948E46CC99EE122596FDB5ED8C0205FB,
	ShopController_Start_m55C7E8A993F5C4F493FC48EE067711BFC57246E9,
	ShopController_BuyBttn_mA1236073BDB22FA005B184480B4C5A62F1DA054A,
	ShopController_updateCosts_m1F4C3D4FDA99242EC8D721EFA423184788F75995,
	ShopController_BonusPerSec_m0DF804B193DCC1BA89DF882D1FA96FAFD2FDD973,
	ShopController_BonusTimer_m588628945F6E4C46225F5AF23D386D6AB50F814D,
	ShopController__ctor_m8F7837C4274D81D87B0F80181A8899F6B2F1D89C,
	U3CBonusPerSecU3Ed__7__ctor_mB51EC4AACACE6399AA67627C151CE567500964CA,
	U3CBonusPerSecU3Ed__7_System_IDisposable_Dispose_m100D6EFDE3BB4C7041A520BB706BDF8DCBD043F6,
	U3CBonusPerSecU3Ed__7_MoveNext_m44FABE968824F26D7C31C2318F91FC2AFC98D71C,
	U3CBonusPerSecU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32F8CE94DADBD0B73237BDBB88E6C8DBAD092BF0,
	U3CBonusPerSecU3Ed__7_System_Collections_IEnumerator_Reset_m22705D7061BB941315D77A0BD11EB0F7067C4991,
	U3CBonusPerSecU3Ed__7_System_Collections_IEnumerator_get_Current_m63707780812E44139CB2E43B9EE52502FF906B74,
	U3CBonusTimerU3Ed__8__ctor_mAB9CE4CA8BEC4CEF3F4C81486EBB28764BB6C1FA,
	U3CBonusTimerU3Ed__8_System_IDisposable_Dispose_mAB814BA2B5EA4797756E42A154A0939FD72DE392,
	U3CBonusTimerU3Ed__8_MoveNext_m9B883AE257FBAA7536DCDEF158DC864B5D142F9C,
	U3CBonusTimerU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CF0C7A706512AFDEB1B772A2DB5C981607330B1,
	U3CBonusTimerU3Ed__8_System_Collections_IEnumerator_Reset_m018D57EA229817C0FC5BDF4CC893C2FC828C16F3,
	U3CBonusTimerU3Ed__8_System_Collections_IEnumerator_get_Current_mBBD460F888EAF59B85FFD94F2A3342BBFD64807B,
	Item__ctor_m51C5ECE57526347477E1E2A4CF519CF555BC8147,
};
static const int32_t s_InvokerIndices[49] = 
{
	1163,
	1163,
	1998,
	1163,
	991,
	1163,
	1163,
	634,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	1163,
	982,
	1163,
	1132,
	483,
	1163,
	982,
	1163,
	1151,
	1132,
	1163,
	1132,
	982,
	1163,
	1151,
	1132,
	1163,
	1132,
	1163,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	49,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
